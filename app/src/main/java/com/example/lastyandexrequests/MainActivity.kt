package com.example.lastyandexrequests

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.jakewharton.rxbinding3.widget.afterTextChangeEvents
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit

// Файлы по пакетам особо не сортировал, потому что приложение маленькое

class MainActivity : AppCompatActivity() {

    @Suppress("PrivatePropertyName")
    private val TAG = javaClass.simpleName
    private lateinit var filterEventsDisposable: Disposable

    private val viewModel by lazy {
        ViewModelProvider(
            viewModelStore,
            ViewModelProvider.AndroidViewModelFactory.getInstance(application)
        )
            .get(MainViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val adapter = SearchQueryAdapter()
        mainRecycler.adapter = adapter
        setSupportActionBar(toolbar)
        supportActionBar?.title = null
        viewModel.queries.observe(this, Observer { adapter.replaceAll(it) })
        filterEventsDisposable = etFilter.afterTextChangeEvents()
            .debounce(350, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Log.d(TAG, "$it")
                viewModel.filterQueries(it.editable.toString())
            }, { Log.e(TAG, "error", it) })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.toggleUpdate -> {
                viewModel.toggleUpdates()
                val textRes = if (viewModel.updateIsActive) {
                    R.string.update_on
                } else {
                    R.string.update_off
                }
                Toast.makeText(this, textRes, Toast.LENGTH_SHORT).show()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onStart() {
        super.onStart()
        viewModel.startUpdates()
    }

    override fun onStop() {
        super.onStop()
        viewModel.stopUpdates()
    }

    override fun onDestroy() {
        super.onDestroy()
        filterEventsDisposable.dispose()
    }
}
