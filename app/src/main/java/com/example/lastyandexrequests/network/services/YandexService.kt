package com.example.lastyandexrequests.network.services

import com.example.lastyandexrequests.network.responses.LastRequestsResponse
import io.reactivex.Flowable
import retrofit2.http.GET

interface YandexService {
    @GET("last/last20x.xml")
    fun getLastRequests(): Flowable<LastRequestsResponse>
}