package com.example.lastyandexrequests.network.responses

import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root


@Root(name = "page", strict = false)
class LastRequestsResponse @JvmOverloads constructor(
    @field:ElementList(name = "last20x", entry = "item")
    var items: List<String>? = null
)