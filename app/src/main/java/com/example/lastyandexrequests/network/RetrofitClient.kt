package com.example.lastyandexrequests.network

interface RetrofitClient {
    fun <S> createService(sClass: Class<S>): S
}