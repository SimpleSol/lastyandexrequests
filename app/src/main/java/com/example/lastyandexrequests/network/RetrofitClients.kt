package com.example.lastyandexrequests.network

import com.example.lastyandexrequests.BuildConfig
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import java.util.concurrent.TimeUnit

class YandexRetrofitClient : RetrofitClient {

    private val retrofit: Retrofit

    init {
        @Suppress("DEPRECATION")
        retrofit = Retrofit.Builder()
            .baseUrl("https://export.yandex.ru/")
            .client(buildOkHttpClient())
            .validateEagerly(true)
            .addConverterFactory(SimpleXmlConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
    }

    private fun buildOkHttpClient(): OkHttpClient {
        return OkHttpClient().newBuilder().apply {
            connectTimeout(60, TimeUnit.SECONDS)
            readTimeout(60, TimeUnit.SECONDS)
            if (BuildConfig.DEBUG) {
                addInterceptor(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                })
            }
        }.build()
    }

    override fun <S> createService(sClass: Class<S>): S = retrofit.create(sClass)

}