package com.example.lastyandexrequests

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_search_query.view.*

class SearchQueryAdapter : BaseAdapter<SearchQuery, SearchQueryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_search_query, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.apply {
            number.text = (position + 1).toString()
            query.text = item.query
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val number = view.number as TextView
        val query = view.query as TextView
    }

}