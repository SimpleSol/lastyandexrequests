package com.example.lastyandexrequests

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.lastyandexrequests.network.YandexRetrofitClient
import com.example.lastyandexrequests.network.services.YandexService
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit

// Отсюда по-хорошему нужно убрать код получения данных, сортировки и фильтрации
// в отдельные классы (Repository, UseCase, Interactor etc.)
// Я этого делать не стал в целях экономии времени.

class MainViewModel : ViewModel() {

    @Suppress("PrivatePropertyName")
    private val TAG = javaClass.simpleName

    private var updateDisposable: Disposable? = null
    private val mutableQueries: MutableLiveData<List<SearchQuery>> = MutableLiveData()
    private var queriesList = listOf<SearchQuery>()
    private val service = YandexRetrofitClient().createService(YandexService::class.java)
    private var updIsActive = true
    private var queriesFilter = ""

    val updateIsActive
        get() = updIsActive
    val queries: LiveData<List<SearchQuery>> = mutableQueries

    fun startUpdates() {
        if (updIsActive.not()) {
            return
        }
        updateDisposable = Flowable.interval(1000, TimeUnit.MILLISECONDS)
            .onBackpressureDrop { Log.d(TAG, "dropped") }
            .flatMap { service.getLastRequests() }
            .retry()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Log.d(TAG, "$it")
                handleQueries(it.items.orEmpty())
            }, { Log.e(TAG, "error", it) })
    }

    fun filterQueries(filter: String) {
        queriesFilter = filter
        handleQueries(null)
    }

    private fun handleQueries(queryStrings: List<String>?) {
        if (queryStrings != null) {
            queriesList = queryStrings.map { SearchQuery(it) }.sortedWith(Comparator { a, b ->
                a.query.compareTo(b.query, true)
            })
        }
        mutableQueries.value = if (queriesFilter.isBlank()) {
            queriesList
        } else {
            queriesList.filter { it.query.contains(queriesFilter, true) }
        }
    }

    fun stopUpdates() {
        clearDisposables()
    }

    fun toggleUpdates() {
        if (updIsActive) {
            updIsActive = false
            stopUpdates()
        } else {
            updIsActive = true
            startUpdates()
        }
    }

    override fun onCleared() {
        clearDisposables()
    }

    private fun clearDisposables() {
        updateDisposable?.dispose()
        updateDisposable = null
    }

}