package com.example.lastyandexrequests

import androidx.recyclerview.widget.RecyclerView


abstract class BaseAdapter<ModelT, ViewHolderT : RecyclerView.ViewHolder> : RecyclerView.Adapter<ViewHolderT>() {

    protected val items = mutableListOf<ModelT>()

    val isEmpty: Boolean
        get() = items.isEmpty()

    override fun getItemCount(): Int {
        return items.size
    }

    fun getItem(position: Int): ModelT? {
        return if (position < 0 || position >= items.size) null else items[position]
    }

    @JvmOverloads
    fun add(item: ModelT, startPosition: Int = items.size) {
        var startPos = startPosition
        if (startPos > items.size) {
            startPos = items.size
        }
        val initialSize = this.items.size
        items.add(startPos, item)
        if (initialSize == 0) {
            notifyDataSetChanged()
        } else {
            notifyItemInserted(startPos)
        }
    }

    fun addAll(items: List<ModelT>?, startPosition: Int) {
        if (items == null || items.isEmpty()) {
            return
        }
        val initialSize = this.items.size
        this.items.addAll(startPosition, items)
        if (initialSize == 0) {
            notifyDataSetChanged()
        } else {
            notifyItemRangeInserted(startPosition, items.size)
        }
    }

    open fun addAll(items: List<ModelT>?) {
        addAll(items, this.items.size)
    }

    fun replace(item: ModelT, position: Int): ModelT? {
        var pos = position
        if (pos > items.size) {
            pos = items.size
        }
        val oldItem = items.removeAt(pos)
        items.add(pos, item)
        notifyItemChanged(pos)
        return oldItem
    }

    fun replaceAll(items: List<ModelT>) {
        clear()
        addAll(items)
    }

    fun clear() {
        val size = items.size
        items.clear()
        notifyItemRangeRemoved(0, size)
    }

    fun remove(position: Int): ModelT? {
        if (position < 0 || position >= items.size) {
            return null
        }
        val removed = items.removeAt(position)
        if (items.isEmpty()) {
            notifyDataSetChanged()
        } else {
            notifyItemRemoved(position)
        }
        return removed
    }

}